#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include "helperfunctions.h"
#include "config.h"

/*
#include <SmartThingsESP8266WiFi.h>
*/

#define RELAYPin D4

// Replace with your network credentials
const char *ssid = SSID;
const char *password = WIFIPASSWORD;

ESP8266WebServer server(80); //instantiate server at port 80 (http port)

String page = "";
void setup(void)
{
	//the HTML of the web page
	page = "<h1>Alimentador de perros</h1><p><a href=\"MotorOn\"><button>ON</button></a>&nbsp;<a href=\"MotorOff\"><button>OFF</button></a></p>";
	//make the LED pin output and initially turned off
	pinMode(RELAYPin, OUTPUT);
	digitalWrite(RELAYPin, LOW);

	delay(1000);
	Serial.begin(115200);
	WiFi.begin(ssid, password); //begin WiFi connection
	Serial.println("");

	// Wait for connection
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		Serial.print(".");
	}
	Serial.println("");
	Serial.print("Connected to ");
	Serial.println(ssid);
	Serial.print("IP address: ");
	Serial.println(WiFi.localIP());

	server.on("/", []() {
		server.send(200, "text/html", page);
	});
	server.on("/MotorOn", []() {
		server.send(200, "text/html", page);
		digitalWrite(RELAYPin, HIGH);
		delay(1000);
	});
	server.on("/MotorOff", []() {
		server.send(200, "text/html", page);
		digitalWrite(RELAYPin, LOW);
		delay(1000);
	});
	server.begin();
	Serial.println("Web server started!");
}

void loop(void)
{
	server.handleClient();
}